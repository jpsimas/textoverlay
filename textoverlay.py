# textOverlay
# Copyright (C) 2020  João Pedro de O Simas

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os, tkinter
if os.name == 'nt':
    import win32api, win32con, pywintypes, pyHook
else:
    import pyxhook as pyHook

class textOverlay:
    def __init__(self, filenames, tk):
        self.filenames = filenames

        self.index = 0;

        self.tk = tk
        
        self.hm = pyHook.HookManager()
        self.hm.KeyDown = self.OnKeyboardEvent
        # set the hook
        self.hm.HookKeyboard()
        self.hm.start()

        self.notes = tkinter.StringVar()
        
        self.label = tkinter.Label(self.tk, textvariable=self.notes, font=('Times New Roman','11'), fg='black', bg='white', justify='left')
        self.label.master.overrideredirect(True)
        self.label.master.geometry("+0+0")
        self.label.master.wm_attributes("-topmost", True)
        self.label.master.lift()
        self.label.master.withdraw()
        
        # ##windows stuff
        if os.name == 'nt':
            label.master.wm_attributes("-disabled", True)
            self.label.master.wm_attributes("-transparentcolor", "white")
            hWindow = pywintypes.HANDLE(int(label.master.frame(), 16))
            # # http://msdn.microsoft.com/en-us/library/windows/desktop/ff700543(v=vs.85).aspx
            # # The WS_EX_TRANSPARENT flag makes events (like mouse clicks) fall through the window.
            exStyle = win32con.WS_EX_COMPOSITED | win32con.WS_EX_LAYERED | win32con.WS_EX_NOACTIVATE | win32con.WS_EX_TOPMOST | win32con.WS_EX_TRANSPARENT
            win32api.SetWindowLong(hWindow, win32con.GWL_EXSTYLE, exStyle)
        
    def do_stuff(self):
        if self.index == len(self.filenames):
            print("Hiding Window");
            self.index = 0
            self.label.master.withdraw()
        else:
            print("Displaying file " + str(self.index) + ": " + filenames[self.index]);

            f = open(self.filenames[self.index], "r")
            strn = f.read()
            self.notes.set(strn)
            f.close()
            
            self.label.master.update()
            self.label.master.deiconify()
            self.label.pack()
            self.index += 1
            
    def OnKeyboardEvent(self, event):
        if(event.Ascii == 197):##F8
            self.do_stuff()
            

tk = tkinter.Tk()

filenames = ["notes0.txt", "notes1.txt", "notes2.txt"]
to = textOverlay(filenames, tk)

tk.wait_visibility(tk)

tk.mainloop()
